# Install
```bash
curl -O https://gitlab.com/itkg-vpare/sel-prox/-/raw/master/prox.tgz
tar -xvf prox.tgz
docker pull mitmproxy/mitmproxy:5.0.1
```

# Launch
```bash
docker run -d --rm --init --name mitmproxy --network nginx-proxy -p 8080:8080 -p 8081:8081 mitmproxy/mitmproxy:5.0.1 mitmweb --web-iface 0.0.0.0
```

# Use
```bash
time web/mitm/prox on
time web/mitm/prox off
```

# Troubleshooting
http://`<docker host IP>`:8081/
```bash
ip -o -4 a
```